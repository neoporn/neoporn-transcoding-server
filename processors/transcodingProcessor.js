const { spawn } = require('child_process')
const mkdirp = require('mkdirp-promise')
const videosModel = require('../models/videosModel.js')

module.exports = async function (job) {


    let { dataVideo, member, file } = job.data

    let framesTotal
    let thumbnailsTotal
    await mkdirp(__dirname + "/../public/members/" + member._id + "/videos/thumbnails/" + dataVideo._id + "/")
    const framesCount = spawn(`ffprobe -v error -count_frames -select_streams v:0 -show_entries stream=nb_read_frames -of default=nokey=1:noprint_wrappers=1 ${__dirname}/../public/uploadTemp/${file}`, {
        shell: true
    })

    framesCount.stdout.on('data', data2 => {
        console.log(`stdout: ${data2}`)
        framesTotal = data2
    })

    framesCount.stderr.on('data', data2 => {
        console.log(`stderr: ${data2}`)
    })

    framesCount.on('close', async code => {
        thumbnailsTotal = Math.floor(framesTotal / 10)
        console.log(`child process exited with code ${code}`)
        const watermark = spawn(`ffmpeg -i ${__dirname}/../public/uploadTemp/${file} -vf "scale=1280:720:force_original_aspect_ratio=decrease,pad=1280:720:(ow-iw)/2:(oh-ih)/2, drawtext=fontfile=statics/fonts/arlrdbd.ttf: fontcolor=white: fontsize=24: x=w-tw-66:y=h-th-30: text='${member.username} on Neoporn'" -c:v libx264 -crf 28 -preset fast -y ${__dirname}/../public/members/${member._id}/videos/${file}.mp4 && ffmpeg -i ${__dirname}/../public/members/${member._id}/videos/${file}.mp4 -vf "select='not(mod(n,${thumbnailsTotal}))', scale=320:-1" -vsync vfr -qscale:v 1 ${__dirname}/../public/members/${member._id}/videos/thumbnails/${dataVideo._id}/img_%02d.jpg && rm ${__dirname}/../public/uploadTemp/${file}`, {
            shell: true
        })

        watermark.stdout.on('data', data => {
            console.log(`stdout: ${data}`)
        })

        watermark.stderr.on('data', data => {
            console.log(`stderr: ${data}`)
        })

        watermark.on('close', code => {
            console.log(`child process exited with code ${code}`)
            videosModel.updateOne({ _id: dataVideo._id }, { $set: { transcoded: true } }).exec()
            return Promise.resolve()
        })
    })
}
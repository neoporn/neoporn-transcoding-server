// Dependencies
require('dotenv').config()
const redirectToHttps = require('./redirectToHttps')
const express = require('express')
const bodyParser = require('body-parser')
const path = require('path')
const cookieParser = require('cookie-parser')
const jwt = require('jsonwebtoken')
const app = express()
const fs = require('fs')
const mkdirp = require('mkdirp-promise')
const http = require('http')
const https = require('https')
const socketIO = require('socket.io')
const multipart = require('connect-multiparty')
const axios = require('axios')
const { forEach } = require('p-iteration')
var resumable = require('./resumable-node.js')((__dirname + "/public/uploadTemp"))
var router = express.Router()
const mailgun = require('mailgun-js')({ apiKey: "d4aedcbf843b49d91d95efe21747abd9-e44cc7c1-53f5dd1a", domain: "mg.intimy.shop" })
const Queue = require('bull')
const { spawn } = require('child_process')
const pid = process.pid
const cluster = require('cluster')
const archiver = require('archiver')

const env = process.env.NODE_ENV || 'development'

// Models
const membersModel = require('./models/membersModel.js')
const videosModel = require('./models/videosModel.js')
const followsModel = require('./models/followsModel.js')
const notificationsModel = require('./models/notificationsModel.js')

app.use(express.static(__dirname + '/public'))
app.use(express.static(__dirname + '/statics'))

app.use(multipart())

let server

var options = {
  key: fs.readFileSync("keys/private.key"),
  cert: fs.readFileSync("keys/__neoporn_net.crt"),
  ca: [
    fs.readFileSync('keys/__neoporn_net.ca-bundle')
  ]
}

if (env === "development") {
  server = http.createServer(app)
}

else {
  server = https.createServer(options, app)
  redirectToHttps()
}

const io = socketIO(server)

app.use(function (req, res, next) {
  let origin = req.headers.origin
  let originAllowed = ["https://neoporn.net", "https://www.neoporn.net", "https://transcoding.neoporn.net", "https://www.transcoding.neoporn.net", "http://localhost:8081", "http://localhost:8082", "http://localhost:3000", "https://www.intimy.shop", "https://intimy.shop"]
  if (originAllowed.indexOf(origin) != -1) {
    res.setHeader("Access-Control-Allow-Origin", origin)
  }
  res.header("Access-Control-Allow-Credentials", true)
  res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS")
  res.header('Access-Control-Allow-Headers', 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, Authorization')
  res.header("Access-Control-Max-Age", "1728000")
  if (req.method == "OPTIONS") {
    res.sendStatus(200)
    res.end()
  }
  else {
    next()
  }
})

app.use(bodyParser.urlencoded({ limit: "50mb", extended: false }))
app.use(bodyParser.json({ limit: "50mb" }))
app.use(cookieParser())

// retrieve file id. invoke with /fileid?filename=my-file.jpg
app.get('/fileid', function (req, res) {
  if (!req.query.filename) {
    return res.status(500).end('query parameter missing')
  }
  // create md5 hash from filename
  res.end(
    crypto.createHash('md5')
      .update(req.query.filename)
      .digest('hex')
  )
})

// Handle uploads through Resumable.js
app.post('/api/uploadvideo', function (req, res) {
  resumable.post(req, async function (status, filename, original_filename, identifier) {
    //when all chunks are uploaded then status equals to "done" otherwise "partly_done"
    if (status === 'done') {
      //when all chunks uploaded, then createWriteStream to /uploads folder with filename
      var stream = fs.createWriteStream('./public/uploadTemp/' + identifier)
      resumable.write(identifier, stream)
      stream.on('data', function (data) { })
      stream.on('end', function () {
        stream.end()
      })
      stream.on('finish', async function (data) {
        //delete chunks after original file is re-created.
        resumable.clean(identifier)
      })
    }
    res.send(status)
  })
})

// Handle status checks on chunks through Resumable.js
app.get('/api/uploadvideo', function (req, res) {
  resumable.get(req, function (status, filename, original_filename, identifier) {
    console.log('GET', status)
    res.status((status == 'found' ? 200 : 204)).send(status)
  })
})

// Handle uploads through Resumable.js
app.post('/api/uploadvideovideoset', async function (req, res) {
  resumable.post(req, async function (status, filename, original_filename, identifier) {
    //when all chunks are uploaded then status equals to "done" otherwise "partly_done"
    if (status === 'done') {
      let ext = identifier.match(/mp4|avi|mov|wmv|ogg/)
      let newIdentifier = identifier.replace(/mp4|avi|mov|wmv|ogg/, "." + ext)
      //when all chunks uploaded, then createWriteStream to /uploads folder with filename
      await mkdirp("public/uploadTemp/" + req.body.userId + "/")
      var stream = fs.createWriteStream('./public/uploadTemp/' + req.body.userId + '/' + newIdentifier)
      resumable.write(identifier, stream)
      stream.on('data', function (data) { })
      stream.on('end', function () {
        stream.end()
      })
      stream.on('finish', async function (data) {
        //delete chunks after original file is re-created.
        resumable.clean(identifier)
      })
    }
    res.send(status)
  })
})

// Handle status checks on chunks through Resumable.js
app.get('/api/uploadvideovideoset', function (req, res) {
  resumable.get(req, function (status, filename, original_filename, identifier) {
    console.log('GET', status)
    res.status((status == 'found' ? 200 : 204)).send(status)
  })
})

app.get('/download/:identifier', function (req, res) {
  resumable.write(req.params.identifier, res)
})

app.get('/resumable.js', function (req, res) {
  var fs = require('fs')
  res.setHeader("content-type", "application/javascript")
  fs.createReadStream("./resumable.js").pipe(res)
})



var videoQueue = new Queue('transcoding', 'redis://127.0.0.1:6379')

videoQueue.process('job1', 4, async function (job, done) {
  let { dataVideo, member, file } = job.data

  let framesTotal
  let thumbnailsTotal
  await mkdirp("public/members/" + member._id + "/videos/thumbnails/" + dataVideo._id + "/")
  const framesCount = spawn(`ffprobe -v error -count_frames -select_streams v:0 -show_entries stream=nb_read_frames -of default=nokey=1:noprint_wrappers=1 public/uploadTemp/${file}`, {
    shell: true
  })

  framesCount.stdout.on('data', data2 => {
    console.log(`stdout: ${data2}`)
    framesTotal = data2
  })

  framesCount.stderr.on('data', data2 => {
    console.log(`stderr: ${data2}`)
  })

  framesCount.on('close', async code => {
    thumbnailsTotal = Math.floor(framesTotal / 10)
    console.log(`child process exited with code ${code}`)
    const watermark = spawn(`ffmpeg -i public/uploadTemp/${file} -vf "scale=1280:720:force_original_aspect_ratio=decrease,pad=1280:720:(ow-iw)/2:(oh-ih)/2, drawtext=fontfile=statics/fonts/arlrdbd.ttf: fontcolor=white: fontsize=24: x=w-tw-66:y=h-th-30: text='${member.username} on Neoporn'" -c:v libx264 -crf 20 -preset slow -y public/members/${member._id}/videos/${file}.mp4 && ffmpeg -i public/members/${member._id}/videos/${file}.mp4 -vf "select='not(mod(n,${thumbnailsTotal}))', scale=320:-1" -vsync vfr -qscale:v 1 public/members/${member._id}/videos/thumbnails/${dataVideo._id}/img_%02d.jpg && rm public/uploadTemp/${file}`, {
      shell: true
    })

    watermark.stdout.on('data', data => {
      console.log(`stdout: ${data}`)
    })

    watermark.stderr.on('data', data => {
      console.log(`stderr: ${data}`)
    })

    watermark.on('close', code => {
      console.log(`child process exited with code ${code}`)
      axios.post(`${process.env.URL_SERVER}/api/transcodedtrue`, { id: dataVideo._id })
        .catch(error => {
          console.log(error)
        })
      done()
    })
  })
})

var videoQueueIntimy = new Queue('transcodingintimy', 'redis://127.0.0.1:6379')

videoQueueIntimy.process('job2', 4, async function (job, done) {
  let { seller, file } = job.data

  let framesTotal
  let thumbnailsTotal
  await mkdirp("public/intimy/sellers/" + seller + "/profilVideos/thumbnails/" + file + "/")
  const framesCount = spawn(`ffprobe -v error -count_frames -select_streams v:0 -show_entries stream=nb_read_frames -of default=nokey=1:noprint_wrappers=1 public/uploadTemp/${file}`, {
    shell: true
  })

  framesCount.stdout.on('data', data2 => {
    console.log(`stdout: ${data2}`)
    framesTotal = data2
  })

  framesCount.stderr.on('data', data2 => {
    console.log(`stderr: ${data2}`)
  })

  framesCount.on('close', async code => {
    thumbnailsTotal = Math.floor(framesTotal / 10)
    console.log(`child process exited with code ${code}`)
    const watermark = spawn(`ffmpeg -i public/uploadTemp/${file} -vsync 2 -c:v libx264 -crf 23 -preset veryfast -y public/intimy/sellers/${seller}/profilVideos/${file}.mp4 && ffmpeg -i public/intimy/sellers/${seller}/profilVideos/${file}.mp4 -vf "select='not(mod(n,${thumbnailsTotal}))', scale=150:153:force_original_aspect_ratio=decrease,pad=150:153:(ow-iw)/2:(oh-ih)/2:#cccccc, format=rgb24" -vsync 2 -qscale:v 1 public/intimy/sellers/${seller}/profilVideos/thumbnails/${file}/img_%02d.jpg && rm public/uploadTemp/${file}`, {
      shell: true
    })

    watermark.stdout.on('data', data => {
      console.log(`stdout: ${data}`)
    })

    watermark.stderr.on('data', data => {
      console.log(`stderr: ${data}`)
    })

    watermark.on('close', code => {
      console.log(`child process exited with code ${code}`)
      axios.post(`${process.env.URL_INTIMY_SERVER}/api/transcoded`, { seller: seller, fileName: file })
        .catch(error => {
          console.log(error)
        })
      done()
    })
  })
})

app.post('/api/transcodevideo', (req, res) => {
  let { dataVideo, member, file } = req.body
  videoQueue.add('job1', { dataVideo, member, file }, { removeOnComplete: true, attempts: 3 })
  res.send("Transcoded")
})

app.post('/api/transcodevideointimy', (req, res) => {
  let { seller, file } = req.body
  videoQueueIntimy.add('job2', { seller, file }, { removeOnComplete: true, attempts: 3 })
  res.send("Transcoded")
})

app.post('/api/postvideoset', async (req, res) => {
  if (req.body.videosLength.length > 0) {
    let str = req.body.formDataVideos.name
    let i = 0, strLength = str.length
    for (i; i < strLength; i++) {
      str = str.replace(" ", "_")
    }
    let randomNumber = Math.floor((Math.random() * 100) + 1) + new Date().getTime().toString()
    await mkdirp("public/intimy/sellers/" + req.body.seller + "/videosSets/")
    var output = fs.createWriteStream("public/intimy/sellers/" + req.body.seller + "/videosSets/" + str + "_" + randomNumber + ".zip")
    var archive = archiver('zip', {
      zlib: { level: 9 } // Sets the compression level.
    })

    // listen for all archive data to be written
    // 'close' event is fired only when a file descriptor is involved
    output.on('close', function () {
      console.log(archive.pointer() + ' total bytes')
      console.log('archiver has been finalized and the output file descriptor has closed.')
      spawn(`rm -rf public/uploadTemp/${req.body.seller}`, {
        shell: true
      })
      axios.post(`${process.env.URL_INTIMY_SERVER}/api/videosetadded`, { seller: req.body.seller, fileName: str + "_" + randomNumber + ".zip", formDataVideos: req.body.formDataVideos, videosLength: req.body.videosLength })
        .catch(error => {
          console.log(error)
        })
      res.send("Ok")
    })

    // This event is fired when the data source is drained no matter what was the data source.
    // It is not part of this library but rather from the NodeJS Stream API.
    // @see: https://nodejs.org/api/stream.html#stream_event_end
    output.on('end', function () {
      console.log('Data has been drained')
    })

    // good practice to catch warnings (ie stat failures and other non-blocking errors)
    archive.on('warning', function (err) {
      if (err.code === 'ENOENT') {
        console.log(err)
      } else {
        // throw error
        throw err
      }
    })

    // good practice to catch this error explicitly
    archive.on('error', function (err) {
      throw err
    })

    // pipe archive data to the file
    archive.pipe(output)
    archive.directory(`public/uploadTemp/${req.body.seller}/`, `${req.body.formDataVideos.name}`)
    archive.finalize()
  }
  else res.send("No videos")
})

app.post('/api/videorejected', (req, res) => {
  const videoRejected = spawn(`rm public/members/${req.body.memberId}/videos/${req.body.fileName}.mp4`, {
    shell: true
  })

  videoRejected.stdout.on('data', data2 => {
    console.log(`stdout: ${data2}`)
  })

  videoRejected.stderr.on('data', data2 => {
    console.log(`stderr: ${data2}`)
  })

  videoRejected.on('close', code => {
    console.log(`child process exited with code ${code}`)
    res.send('Video deleted')
  })
})

app.get('/api/authtoken2', function (req, res) {
  jwt.verify(req.cookies.authtoken2, 'supersecret', function (err, decoded) {
    res.json({ token: decoded, status: true })
  })
})

const port = process.env.PORT || 8082

server.listen(port, (req, res) => {
  console.log(`Started process ${pid}`);
})
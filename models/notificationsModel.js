const mongoose = require('mongoose')

let Schema = mongoose.Schema

const verificationVideo = new Schema({
    videoId: String,
    stateVerification: Boolean,
    reason: String
})

const followedNewVideo = new Schema({
  videoId: String,
  uploaderId: String
})

const schema = new Schema({
  //Commons Attributes
  notifType: {
    type: String,
    required: true
  },
  userId: String,
  seen: {
    type: Boolean,
    required: true,
    default: false
  },
  date: {
    type: Date,
    default: Date.now
  },
  //Notif types data
  verificationVideo: verificationVideo,
  followedNewVideo: followedNewVideo
})

const model = mongoose.model('Notification', schema)

module.exports = model
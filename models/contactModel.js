const mongoose = require('mongoose')

let Schema = mongoose.Schema

const schema = new Schema({
  userEmail: {
    type: String,
    required: true
  },
  subject: {
    type: String,
    required: true
  },
  message: {
    type: String,
    required: true
  },
  userIp: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  }
}, {usePushEach: true})

const model = mongoose.model('Contact', schema)

module.exports = model
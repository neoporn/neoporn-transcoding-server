const mongoose = require('mongoose')

let Schema = mongoose.Schema

const schema = new Schema({
  email: {
    type: String,
    required: true
  },
  username: {
    type: String,
    required: true
  },
  profilPicture: String,
  password: {
    type: String,
    required: true
  },
  role: {
    type: Number,
    default: 0
  },
  emailVerified: {
    type: Boolean,
    default: false
  },
  followers: {
    type: Number,
    default: 0
  },
  following: {
    type: Number,
    default: 0
  },
  lastConnection: {
    type: Date,
    default: Date.now
  },
  lastActivity: Date,
  date: {
    type: Date,
    default: Date.now
  }
}, {usePushEach: true})

const model = mongoose.model('Member', schema)

module.exports = model
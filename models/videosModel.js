const mongoose = require('mongoose')

let Schema = mongoose.Schema

const schema = new Schema({
  title: {
    type: String,
    required: true
  },
  description: {
    type: String
  },
  views: {
    type: Number,
    default: 0
  },
  likes: {
    type: Number,
    default: 0
  },
  dislikes: {
    type: Number,
    default: 0
  },
  url: {
    type: String,
    required: true
  },
  urlThumbnail: {
    type: String,
    required: false
  },
  uploader: {
    type: String,
    required: true 
  },
  verified: {
    type: Boolean,
    default: false
  },
  authorized: {
    type: Boolean,
    default: false
  },
  transcoded: {
    type: Boolean,
    default: false
  },
  date: {
    type: Date,
    default: Date.now
  }
}, {usePushEach: true})

const model = mongoose.model('Video', schema)

module.exports = model
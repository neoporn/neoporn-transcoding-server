const mongoose = require('mongoose')

let Schema = mongoose.Schema

const schema = new Schema({
  status: {
    type: Boolean,
    default: false
  },
  userIp: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  }
}, {usePushEach: true})

const model = mongoose.model('SafeMode', schema)

module.exports = model
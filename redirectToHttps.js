module.exports = () => {
    let app = require('express')()

    let server = require('http').createServer(app)
    
    app.use((req, res) => {
        res.redirect(301, `${process.env.ORIGIN}${req.originalUrl}`)
    })
    
    server.listen('80')
}